﻿using System;

namespace printSquare
{
    class Program
    {
        private static int GetUserInput()
        {
            string completeString = Console.ReadLine();
            Int32.TryParse(completeString, out int stringSize);

            return stringSize;
        }

        private static string GetSquareWidthString(int squareWidth)
        {
            string completeString = "";
            for (int i = 0; i < squareWidth; i++)
            {
                completeString += "#";
            }
            return completeString;
        }

        private static string GetSquareHeigthString(int squareHeigth)
        {
            string completeString = "#";
            for (int i = 2; i < squareHeigth; i++)
            {
                completeString += " ";
            }
            completeString += "#";

            return completeString;
        }

        private static void PrintSquare(string stringHeight, string stringWidth, int amountOfTimes)
        {
            Console.WriteLine(stringWidth);
            for (int i = 2; i < amountOfTimes; i++)
            {
                Console.WriteLine(stringHeight);
            }
            Console.WriteLine(stringWidth);
        }

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Type the squares width");
                int squareWidth = GetUserInput();

                Console.WriteLine("Type squares heigth");
                int squareHeight = GetUserInput();

                string stringWidthToPrint = GetSquareWidthString(squareWidth);
                string stringHeightToPrint = GetSquareHeigthString(squareHeight);

                PrintSquare(stringHeightToPrint, stringWidthToPrint, squareHeight);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
